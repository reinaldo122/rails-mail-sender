class User
  def initialize(name, email, login)
    @name  = name
    @email = email
    @login = login
  end

  def email
    @email
  end

  def name
    @name
  end

  def login
    @login
  end
end
