class UserMailer < ApplicationMailer
  default from: "notification@mysuperapp.com"

  def welcome_email(user)
    @user = user
    mail(to: @user.email, subject: "Welcome to the jungle")
  end
end
