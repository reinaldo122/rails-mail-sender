class ApplicationMailer < ActionMailer::Base
  default from: "daniel.esparcha@gmail.com"
  layout 'mailer'
end
